import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'event-emitter';

  public mesajVer():void 
  {
    console.log('Mesaj');
  }

  public showCounter(count):void {
    console.log('counter :' + count);
  }
}
