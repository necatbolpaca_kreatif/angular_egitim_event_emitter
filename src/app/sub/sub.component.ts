import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from 'events';

@Component({
  selector: 'app-sub',
  template: `<button (click)="mesajVer()">Mesaj ver</button>`,
  styleUrls: ['./sub.component.css']
})
export class SubComponent implements OnInit {

  constructor() {
    this.counter = 0;
   }

  @Output() valueChange = new EventEmitter();
  public mesajVer():void {
    console.log('mesaj')
  }
  counter: number;

  public valueChanged(): void {
    this.counter++;
    this.valueChange.emit(this.counter);
  }

  ngOnInit() {
  }

}
